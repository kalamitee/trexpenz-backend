from flask import Flask
from flask_cors import CORS
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy import MetaData

from .config import config_by_name, api_version

convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

metadata = MetaData(naming_convention=convention)

db = SQLAlchemy(metadata=metadata)
api = Api(prefix='/api/' + api_version)
migrate = Migrate(compare_type=True)

from .main.user.user_model import User as UserModel
from .main.budget.budget_model import Budget as BudgetModel, BudgetBreakdown as BudgetBreakdownModel
from .main.expense.expense_model import ExpenseType as ExpenseTypeModel, Expense as ExpenseModel

from .main.auth.auth_endpoint import Auth
from .main.user.user_endpoint import UserList, User, UserChangePassword
from .main.budget.budget_endpoint import BudgetList, Budget, BudgetBreakdown
from .main.expense.expense_endpoint import ExpenseTypeList, ExpenseList, Expense, ExpenseType
from .main.utils.endpoints import UserBudgetList, UserBudget, UserExpenseList, UserExpenseTypeList, BudgetExpenseList, BudgetBreakdownExpenseList, ExpenseTypeExpenseList, UserExpenseType, UserExpense
from .main._playground.hello.hello_endpoint import Hello
from .main._db.db_endpoint import DbEndpoint


def create_app(config_name='dev'):
    app = Flask(__name__)
    cors = CORS(app)

    app.config.from_object(config_by_name[config_name])

    db.init_app(app)
    migrate.init_app(app, db)

    api.add_resource(Auth, '/auth')

    api.add_resource(UserList, '/users')
    api.add_resource(UserExpenseList, '/users/<id>/expenses')
    api.add_resource(UserBudgetList, '/users/<id>/budgets')
    api.add_resource(UserBudget, '/users/<id>/budgets', '/users/<user_id>/budgets/<budget_id>')
    api.add_resource(UserExpenseTypeList, '/users/<id>/expense-types')
    api.add_resource(UserExpenseType, '/users/<user_id>/expense-types', '/users/<user_id>/expense-types/<expense_type_id>')
    api.add_resource(UserExpense, '/users/<id>/expenses', '/users/<user_id>/expenses/<expense_id>')
    api.add_resource(UserChangePassword, '/cred')

    api.add_resource(ExpenseTypeList, '/expense-types')
    api.add_resource(ExpenseList, '/expenses')
    api.add_resource(Expense, '/expenses/<id>')
    api.add_resource(ExpenseType, '/expense-types/<id>')
    api.add_resource(ExpenseTypeExpenseList, '/expense-types/<id>/expenses')

    api.add_resource(BudgetExpenseList, '/budgets/<id>/expenses')
    api.add_resource(BudgetBreakdownExpenseList, '/budget-breakdown/<id>/expenses')
    api.add_resource(BudgetList, '/budgets')
    api.add_resource(BudgetBreakdown, '/budget-breakdown')
    api.add_resource(Budget, '/budgets/<id>')

    api.add_resource(Hello, '/hello')
    api.add_resource(DbEndpoint, '/_db')

    api.init_app(app)

    return app
