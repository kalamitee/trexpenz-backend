class EditError(Exception):
    pass

class BudgetBreakdownError(Exception):
    pass

class ApiFilterError(Exception):
    pass
    