from flask import jsonify, make_response
from werkzeug.http import HTTP_STATUS_CODES

from app import db

def error_response(status_code, message=None):
    payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}

    if message:
        payload['message'] = message
    response = jsonify(payload)

    return make_response(response, status_code)

def get_not_found_message(cls, id):
    return cls.__name__ + ' with id ' + str(id) + ' not found'

def integrity_error(err, additional_detail=None):
    error_msg_index = err.__cause__.__str__().find('DETAIL')
    error_msg = err.__cause__.__str__()[error_msg_index:]
    
    db.session.rollback()

    response = {'error': error_msg}

    if additional_detail:
        response['additional_Detail'] = additional_detail

    return make_response(jsonify(response), 422)
