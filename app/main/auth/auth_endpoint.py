from flask_restful import Resource, request

from ..user.user_schema import AccountSchema
from ..utils.decorators import validate_request, token_required
from .auth_service import AuthService


class Auth(Resource):

    method_decorators = {
        'delete': [token_required],
        'post': [validate_request(AccountSchema())]
    }

    def post(self):
        return AuthService.login_user()

    def delete(self):
        return AuthService.logout_user()
