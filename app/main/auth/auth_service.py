import datetime
from flask import make_response, jsonify, g, request

from app import db
from ..errors import error_response
from ..user.user_model import User as UserModel, AccountToken
from ..user.user_schema import UserSchema


class AuthService:

    @staticmethod
    def login_user(user_data=None):
        data = request.get_json() if user_data is None else user_data

        # data = request.get_json()
        user = UserModel.find_account_by_username(data.get('username'))

        if not user:
            return error_response(401, 'User does not exist')

        if user.account.password_matched(data.get('password')):
            # auth_token = UserModel.generate_token(user.id)
            auth_token = user.account.generate_token()
            g.current_user = user

            resp = {
                'data': {
                    'user': UserSchema(check_envelope=False).dump(user).data,
                    'auth_token': auth_token
                }
            }

            return make_response(jsonify(resp), 200)

        return error_response(401, 'Invalid password')

    @staticmethod
    def logout_user():
        token = request.headers.get('Authorization').split(' ')[1]
        account_token = AccountToken.query.filter_by(token=token, account_id=g.current_user.account.id).first()

        g.current_user = None
        db.session.delete(account_token)
        db.session.commit()

        return make_response(jsonify({}), 204)
