import simplejson
from decimal import ROUND_HALF_UP

from marshmallow import Schema, fields, validates, ValidationError

from app.main.utils.validators import must_not_be_blank


class HelloSchema(Schema):
    class Meta:
        json_module = simplejson

    id = fields.Integer(dump_only=True)
    name = fields.String(required=True, validate=must_not_be_blank)
    date_created = fields.Date(format='%Y-%m-%d', dump_only=True)
    amount = fields.Decimal(required=True, places=2, rounding=ROUND_HALF_UP, validate=must_not_be_blank)

    @validates('amount')
    def validate_amount(self, value):
        if value == 100:
            raise ValidationError('Your value is 100 idiot')
    