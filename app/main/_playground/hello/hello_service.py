import datetime as dt
from decimal import Decimal

from flask import request, make_response, jsonify

from app import db
from app.main.errors import get_not_found_message
from .hello_model import Hello as HelloModel
from .hello_schema import HelloSchema

class HelloService:

    @staticmethod
    def save_hello():
        json_data = request.get_json()
        schema = HelloSchema()

        loaded = schema.load(json_data).data

        print('is INSTANCE ', loaded)

        if isinstance(json_data, list):
            to_create = [d for d in json_data if 'id' not in d.keys()]
            to_update = [d for d in json_data if 'id' in d.keys()]

            created_list = HelloService.create_hello_list(to_create)
            updated_list = HelloService.update_hello_list(to_update)

            db.session.commit()

            return make_response(jsonify(schema.dump(created_list + updated_list, many=True).data), 200)
        else:
            status_code = None
            response = None

            if json_data.get('id'):
                response = HelloService.update_hello(json_data)
                status_code = 200
            else:
                response = HelloService.create_hello(json_data)
                status_code = 201
            
            db.session.commit()

            result = HelloModel.query.filter(HelloModel.date_created.between('2019-05-21', '2019-05-25')).all()

            print('THE RESUT ', result)

            return make_response(jsonify(schema.dump(response).data), status_code)

    @staticmethod
    def create_hello_list(json_data_list):
        result = []

        for d in json_data_list:
            hello = HelloService.create_hello(d)
            result.append(hello)
        
        return result

    @staticmethod
    def update_hello_list(json_data_list):
        result = []

        for d in json_data_list:
            hello = HelloService.update_hello(d)
            result.append(hello)

        return result

    @staticmethod
    def create_hello(json_data):
        hello = HelloModel.from_config(json_data)

        db.session.add(hello)

        return hello
    
    @staticmethod
    def update_hello(json_data):
        hello = HelloModel.query.get_or_404(json_data.get('id'), description=get_not_found_message(HelloModel, json_data.get('id')))
        hello.name = json_data.get('name')
        # hello.date_created = json_data.get('date_created')
        hello.amount = json_data.get('amount')
        
        return hello
