from flask import request
from flask_restful import Resource

from app import db

from app.main.utils.decorators import token_required, validate_request
from app.main.errors import error_response

from .hello_schema import HelloSchema
from .hello_model import Hello as HelloModel
from .hello_service import HelloService

class Hello(Resource):
    method_decorators = [validate_request(HelloSchema())]

    def get(self):
        return { 'hello': 'letmesendit' }

    def post(self):
        return HelloService.save_hello()
