import datetime as dt

from app import db

from .. ..main.errors import get_not_found_message

class Hello(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    date_created = db.Column(db.Date, nullable=False, index=True)
    amount = db.Column(db.Numeric(), nullable=False, index=True)

    hello_data = ['name', 'amount']

    @classmethod
    def from_config(cls, data):
        if data.get('id'):
            hello = cls.query.get_or_404(data['id'], description=get_not_found_message(cls, data['id']))
        else:
            hello = cls()

        hello.set_data(data)

        return hello
    
    def set_data(self, data):
        if not data.get('id'):
            setattr(self, 'date_created', dt.date.today())

        for field in self.hello_data:
            setattr(self, field, data[field] if field in data else None)
        