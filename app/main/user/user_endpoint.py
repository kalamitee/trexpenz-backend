from flask import request, g, make_response, jsonify
from flask_restful import Resource

from app import db

from .user_schema import UserSchema, UserChangePasswordSchema
from ..utils.decorators import validate_request, token_required, logged_in_user_match_id
from .user_service import UserService
from ..auth.auth_service import AuthService


class User(Resource):

    method_decorators = [token_required]

    def get(self, user_id):
        pass

    def put(self, id):
        pass


class UserList(Resource):

    create_user_schema = UserSchema(partial=('id',))
    update_user_schema = UserSchema(exclude=('account',))

    method_decorators = {
        'post': [validate_request(create_user_schema)],
        'put': [logged_in_user_match_id('id'), token_required, validate_request(update_user_schema)]
    }

    def post(self):
        json_data = request.get_json()

        data = self.create_user_schema.load(json_data).data
        res = UserService.save_user(data)

        if res.status_code == 201:
            account = {
                'username': json_data.get('data').get('account').get('username'),
                'password': json_data.get('data').get('account').get('password')
            }

            return AuthService.login_user(account)
        return res

    def put(self):
        json_data = request.get_json()
        data = self.update_user_schema.load(json_data).data

        return UserService.save_user(data)


class UserChangePassword(Resource):

    user_change_password_schema = UserChangePasswordSchema()

    method_decorators = [token_required]

    def put(self):
        json_data = request.get_json()
        data = self.user_change_password_schema.load(json_data).data

        g.current_user.account.set_new_password(data.get('password'))

        return make_response(jsonify({}), 204)
