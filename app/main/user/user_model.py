import os
import datetime
import base64

from werkzeug.security import generate_password_hash, check_password_hash

from app import db
from ..errors import get_not_found_message


class AccountToken(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    account_id = db.Column(db.Integer, db.ForeignKey('account.id'), nullable=False)
    token = db.Column(db.String(32), index=True, unique=True, nullable=False)
    token_expiration = db.Column(db.DateTime)

    def __repr__(self):
        return '<AccountToken {}>'.format(self.token)

    @classmethod
    def generate_token(cls, expires_in):
        account_token = cls()
        account_token.encode_token(expires_in)

        return account_token

    def revoke_token(self):
        self.token_expiration = datetime.datetime.utcnow() - datetime.timedelta(seconds=1)

    def encode_token(self, expires_in):
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = datetime.datetime.utcnow() + datetime.timedelta(hours=expires_in)
        # self.token_expiration = datetime.datetime.utcnow() + datetime.timedelta(seconds=5)

    def is_token_expired(self):
        return datetime.datetime.utcnow() > self.token_expiration

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def check_token(token):
        account_token = AccountToken.query.filter_by(token=token).first()
        user = account_token.account.user

        if user is None or account_token.expired():
            return None

        return user


class Account(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    user = db.relationship('User', backref='account', uselist=False)
    account_tokens = db.relationship('AccountToken', backref='account', lazy=True)

    def __repr__(self):
        return '<Account {}>'.format(self.username)

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def password_matched(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_token(self, expires_in=6):
        self.account_token = AccountToken.generate_token(expires_in)
        self.account_token.account_id = self.id
        self.account_token.save_to_db()

        return self.account_token.token

    def set_new_password(self, new_password):
        self.password = new_password
        db.session.commit()


class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.Date, index=True, nullable=False)
    first_name = db.Column(db.String(64), index=True, nullable=False)
    last_name = db.Column(db.String(64), index=True)
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)
    account_id = db.Column(db.Integer, db.ForeignKey('account.id'), unique=True, nullable=False)
    budgets = db.relationship('Budget', backref='user', lazy='dynamic')
    expenses = db.relationship('Expense', backref='user', lazy='dynamic')
    expense_types = db.relationship('ExpenseType', backref='user', lazy='dynamic')

    user_data = ['first_name', 'last_name', 'email']

    def __repr__(self):
        return '<User {}>'.format(self.first_name)

    def set_user_data(self, data):
        if data.get('account') and not data.get('id'):
            self.create_account(data['account'])
            setattr(self, 'date_created', datetime.date.today())

        for field in self.user_data:
            setattr(self, field, data[field] if field in data else None)

    def create_account(self, account_data):
        self.account = Account(**account_data)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def from_config(cls, data):
        if data.get('id'):
            user = cls.query.get_or_404(data['id'], description=get_not_found_message(cls, data['id']))
        else:
            user = cls()

        user.set_user_data(data)

        return user

    @classmethod
    def find_account_by_username(cls, username):
        return cls.query.join(Account).filter(
            Account.username == username
        ).first()
