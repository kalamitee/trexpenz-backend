from marshmallow import fields, Schema

from ..base import BaseSchema
from ..utils.validators import must_not_be_blank


class AccountSchema(Schema):

    id = fields.Integer(dump_only=True)
    username = fields.String(required=True, validate=must_not_be_blank)
    password = fields.String(required=True, validate=must_not_be_blank, load_only=True)


class UserSchema(BaseSchema):

    id = fields.Integer(required=True, validate=must_not_be_blank)
    first_name = fields.String(required=True, validate=must_not_be_blank, load_from='firstName', dump_to='firstName')
    last_name = fields.String(load_from='lastName', dump_to='lastName')
    email = fields.Email(required=True)
    account = fields.Nested(AccountSchema, required=True, load_only=True)
    date_created = fields.Date(format='%Y-%m-%d', dump_only=True)

    def __init__(self, check_envelope=True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.check_envelope = check_envelope


class UserChangePasswordSchema(Schema):

    password = fields.String(required=True, validate=must_not_be_blank, load_only=True)
