from flask import jsonify, make_response
from sqlalchemy.exc import IntegrityError

from app import db

from ..errors import integrity_error
from .user_schema import UserSchema
from .user_model import User as UserModel


class UserService:

    @staticmethod
    def save_user(data):
        user = UserModel.from_config(data)

        try:
            user.save_to_db()
        except IntegrityError as err:
            return integrity_error(err)

        return make_response(jsonify(UserSchema().dump(UserModel.query.get(user.id)).data), 201)
