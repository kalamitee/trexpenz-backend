from flask_restful import Resource

from app import db

class DbEndpoint(Resource):

    def post(self):
        db.create_all()

    def delete(self):
        db.drop_all()
        