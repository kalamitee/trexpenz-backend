import datetime

from flask import request

from app import db

from .. .main.errors import get_not_found_message
from ..errors.exceptions import BudgetBreakdownError
from ..user.user_model import User as UserModel
from ..budget.budget_model import Budget as BudgetModel, BudgetBreakdown as BudgetBreakdownModel


class ExpenseType(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)
    name = db.Column(db.String(64), nullable=False, index=True, unique=True)
    is_variable = db.Column(db.Boolean, nullable=False, index=True)
    expenses = db.relationship('Expense', backref='expense_type', lazy='dynamic')

    def set_data(self, data):
        setattr(self, 'name', data.get('name'))
        setattr(self, 'is_variable', data.get('is_variable'))

        user = UserModel.query.get_or_404(data['user']['id'], description=get_not_found_message(UserModel, data['user']['id']))

        setattr(self, 'user', user)

    @classmethod
    def from_config(cls, data):
        if data.get('id'):
            expense_type = cls.query.get_or_404(data.get('id'), description=get_not_found_message(cls, data['id']))
        else:
            expense_type = cls()

        expense_type.set_data(data)

        return expense_type


class Expense(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.Date, nullable=False, index=True)
    budget_id = db.Column(db.Integer, db.ForeignKey('budget.id'), nullable=True, index=True)
    budget_breakdown_id = db.Column(db.Integer, db.ForeignKey('budget_breakdown.id'), nullable=True, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)
    expense_type_id = db.Column(db.Integer, db.ForeignKey('expense_type.id'), nullable=True, index=True)
    name = db.Column(db.String(64), nullable=False, index=True)
    date = db.Column(db.Date, nullable=False, index=True)
    amount = db.Column(db.Numeric(), nullable=False, index=True)
    description = db.Column(db.String(128), index=True)

    def set_data(self, data):
        if not data.get('id'):
            setattr(self, 'date_created', datetime.date.today())

        setattr(self, 'name', data.get('name'))
        setattr(self, 'date', data.get('date'))
        setattr(self, 'amount', data.get('amount'))
        setattr(self, 'description', data.get('description'))

        user = UserModel.query.get_or_404(data['user']['id'], description=get_not_found_message(UserModel, data['user']['id']))
        budget = BudgetModel.query.filter_by(year=str(data.get('date').year), user_id=data['user']['id']).first()
        expense_type = ExpenseType.query.get_or_404(data['expense_type']['id'], description=get_not_found_message(ExpenseType, data['expense_type']['id']))

        print('THE BUDGET ', budget)

        setattr(self, 'user', user)
        setattr(self, 'budget', budget)
        setattr(self, 'expense_type', expense_type)

        budget_breakdown = BudgetBreakdownModel.query.filter_by(month=data.get('date').month, budget_id=self.budget.id).first()
        setattr(self, 'budget_breakdown', budget_breakdown)

    @classmethod
    def from_config(cls, data):
        if data.get('id'):
            expense = cls.query.get_or_404(data.get('id'), description=get_not_found_message(cls, data['id']))
        else:
            expense = cls()

        expense.set_data(data)

        return expense
