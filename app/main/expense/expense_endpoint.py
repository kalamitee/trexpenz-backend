from flask import request, make_response, jsonify, g
from flask_restful import Resource
from sqlalchemy import and_, or_

from app import db

from ..utils import url_b64_to_dict
from ..utils.query_parser import QueryParser
from ..utils.decorators import validate_request, token_required
from ..errors.exceptions import ApiFilterError
from ..errors import error_response
from .expense_schema import ExpenseTypeSchema, ExpenseSchema
from .expense_service import ExpenseTypeService, ExpenseService
from .expense_model import Expense as ExpenseModel, ExpenseType as ExpenseTypeModel


class ExpenseList(Resource):

    expense_schema = ExpenseSchema()

    method_decorators = {
        'get': [token_required],
        'post': [token_required, validate_request(expense_schema)],
        'put': [token_required, validate_request(expense_schema)]
    }

    def get(self):
        q = request.args.get('q')

        try:
            final_query = QueryParser.get_final_query(ExpenseModel, db.session.query(ExpenseModel), q)
            result = final_query.all()
        except ApiFilterError as err:
            return error_response(400, str(err))

        return make_response(jsonify(self.expense_schema.dump(result, many=True).data), 200)

    def post(self):
        json_data = request.get_json()
        data = self.expense_schema.load(json_data).data

        return ExpenseService.save(data)

    def put(self):
        json_data = request.get_json()
        data = self.expense_schema.load(json_data).data

        return ExpenseService.save(data)

    def delete(self, id):
        print('HERE MEN', id)

        return {'haha': 'hehe'}


class Expense(Resource):

    expense_schema = ExpenseSchema()

    method_decorators = {
        'get': [token_required]
    }

    def get(self, id):
        query = db.session.query(ExpenseModel).filter(ExpenseModel.id == id)

        return make_response(jsonify(self.expense_schema.dump(query.first()).data), 200)


class ExpenseTypeList(Resource):

    expense_type_schema = ExpenseTypeSchema()

    method_decorators = {
        'get': [token_required],
        'post': [token_required, validate_request(expense_type_schema)],
        'put': [token_required, validate_request(expense_type_schema)]
    }

    def get(self):
        q = request.args.get('q')

        try:
            final_query = QueryParser.get_final_query(ExpenseTypeModel, db.session.query(ExpenseTypeModel), q)
            result = final_query.all()
        except ApiFilterError as err:
            return error_response(400, str(err))

        return make_response(jsonify(self.expense_type_schema.dump(result, many=True).data), 200)

    def post(self):
        json_data = request.get_json()
        data = self.expense_type_schema.load(json_data).data

        return ExpenseTypeService.save(data)

    def put(self):
        json_data = request.get_json()
        data = self.expense_type_schema.load(json_data).data

        return ExpenseTypeService.save(data)


class ExpenseType(Resource):

    expense_type_schema = ExpenseTypeSchema()

    method_decorators = {
        'get': [token_required],
        'delete': [token_required]
    }

    def get(self, id):
        query = db.session.query(ExpenseTypeModel).filter(ExpenseTypeModel.id == id)

        return make_response(jsonify(self.expense_type_schema.dump(query.first()).data), 200)

    def delete(self, id):

        return ExpenseTypeService.delete(id)
