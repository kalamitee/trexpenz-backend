from flask import jsonify, make_response, g
from sqlalchemy.exc import IntegrityError

from app import db

from ..errors import integrity_error, error_response
from ..errors.exceptions import BudgetBreakdownError
from .expense_schema import ExpenseTypeSchema, ExpenseSchema
from .expense_model import ExpenseType as ExpenseTypeModel, Expense as ExpenseModel


class ExpenseService:

    @staticmethod
    def save(data):
        status_code = None
        response = None
        schema = ExpenseSchema()

        try:
            response = ExpenseModel.from_config(data)

            if data.get('id'):
                status_code = 200
            else:
                status_code = 201
                db.session.add(response)

            db.session.commit()
        except BudgetBreakdownError as err:
            db.session.rollback()
            return error_response(422, str(err))

        return make_response(jsonify(schema.dump(response).data), status_code)


class ExpenseTypeService:

    @staticmethod
    def save(data):
        status_code = None
        response = None
        schema = ExpenseTypeSchema()

        try:
            response = ExpenseTypeModel.from_config(data)

            if data.get('id'):
                status_code = 200
            else:
                db.session.add(response)
                status_code = 201

            db.session.commit()
        except IntegrityError as err:
            return integrity_error(err)

        return make_response(jsonify(schema.dump(response).data), status_code)

    @staticmethod
    def delete(id):
        expense_type = db.session.query(ExpenseTypeModel).filter(ExpenseTypeModel.id == id).first()

        if expense_type is None:
            return make_response(jsonify({'message': ExpenseTypeModel.__name__ + ' does not exist'}), 404)

        if expense_type.user_id != g.current_user.id:
            message = ExpenseTypeModel.__name__ + ' with id = ' + str(id)
            message += ' does not belong to User with id = ' + str(g.current_user.id)

            return make_response(jsonify({'message': message}), 422)

        db.session.delete(expense_type)
        db.session.commit()

        return make_response(jsonify(None), 204)
