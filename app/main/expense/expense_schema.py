import simplejson
from marshmallow import fields

from ..base import BaseSchema
from ..utils.validators import must_not_be_blank
from ..user.user_schema import UserSchema
from ..budget.budget_schema import BudgetSchema, BudgetBreakdownWithBudgetSchema


class ExpenseTypeSchema(BaseSchema):

    id = fields.Integer(allow_none=True)
    user = fields.Nested(UserSchema(check_envelope=False, only=('id',)), required=True, validate=must_not_be_blank, load_only=True)
    name = fields.String(required=True, validate=must_not_be_blank)
    is_variable = fields.Boolean(required=True, load_from='isVariable', dump_to='isVariable')

    def __init__(self, check_envelope=True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.check_envelope = check_envelope


class ExpenseSchema(BaseSchema):

    id = fields.Integer(allow_none=True)
    date_created = fields.Date(format='%Y-%m-%d', dump_only=True)
    name = fields.String(required=True, validate=must_not_be_blank)
    date = fields.Date(format='%Y-%m-%d', required=True, validate=must_not_be_blank)
    amount = fields.Decimal(required=True, validate=must_not_be_blank)
    description = fields.String()

    user = fields.Nested(UserSchema(check_envelope=False, only=('id',)), required=True, validate=must_not_be_blank, load_only=True)
    # budget = fields.Nested(BudgetSchema(check_envelope=False, only=('id',)), required=True, validate=must_not_be_blank)
    # budget_breakdown = fields.Nested(BudgetBreakdownWithBudgetSchema(check_envelope=False, only=('id',)), missing=None)
    budget = fields.Nested(BudgetSchema(check_envelope=False, only=('id',)), required=True, dump_only=True)
    budget_breakdown = fields.Nested(BudgetBreakdownWithBudgetSchema(check_envelope=False, only=('id', 'month',)), dump_only=True, load_from='budgetBreakdown', dump_to='budgetBreakdown')
    expense_type = fields.Nested(ExpenseTypeSchema(check_envelope=False, only=('id', 'name',)), required=True, validate=must_not_be_blank, load_from='expenseType', dump_to='expenseType')
