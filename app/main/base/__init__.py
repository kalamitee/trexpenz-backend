from marshmallow import Schema, pre_load, post_dump, ValidationError

class BaseSchema(Schema):

    __envelope__ = 'data'
    check_envelope = True

    @pre_load(pass_many=True)
    def unwrap_envelope(self, data, many):
        if self.check_envelope:
            if 'data' not in data:
                raise ValidationError('Missing key: data')
            return data.get(self.__envelope__)
        return data

    @post_dump(pass_many=True)
    def wrap_with_envelope(self, data, many):
        if isinstance(data, dict) and not data:
            result = None
        else:
            result = data

        if self.check_envelope:
            return {self.__envelope__: result}
        return data
