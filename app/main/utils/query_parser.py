import json
from operator import itemgetter
from sqlalchemy import and_, or_

from app import db

from ..errors.exceptions import ApiFilterError


class QueryParser:
    @staticmethod
    def get_final_query(model, query, params):
        if params:
            dict_q = json.loads(params)
            query_params = dict_q.get('query')

            if query_params.get('op') == 'and':
                return query.filter(and_(*QueryParser.get_query_list(model, query_params.get('conditions'))))
            else:
                return query.filter(or_(*QueryParser.get_query_list(model, query_params.get('conditions'))))
        else:
            return query

    @staticmethod
    def get_query_list(model, conditions):
        result = []

        for condition in conditions:
            prop, op, value = itemgetter('prop', 'op', 'value')(condition)
            column = getattr(model, prop, None)

            if not column:
                raise ApiFilterError('Invalid filter column: %s' % prop)

            if op == 'in':
                filt = column.in_(value)
            else:
                try:
                    attr = [e for e in ['%s', '%s_', '__%s__'] if hasattr(column, e % op)][0] % op
                except IndexError:
                    raise ApiFilterError('Invalid filter operator: %s' % op)

                if value == 'null':
                    value = None
                filt = getattr(column, attr)(value)
            result.append(filt)
        return result
