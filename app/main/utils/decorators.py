from functools import wraps
from flask import request, g

from ..errors import error_response
from ..user.user_model import User as UserModel, AccountToken


def token_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        data = request.headers.get('Authorization')
        token = None if not data else data.split(' ')[1]

        if (token):
            account_token = AccountToken.query.filter_by(token=token).first()

            if (account_token):
                if (account_token.is_token_expired()):
                    return error_response(401, 'Token is expired')

                g.current_user = account_token.account.user
            else:
                return error_response(401)
        else:
            return error_response(401)

        return fn(*args, **kwargs)
    return wrapper


def validate_request(schema):
    def decorator(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            json_data = request.get_json()
            errors = None

            if isinstance(json_data, list):
                errors = schema.validate(json_data, many=True)
            else:
                errors = schema.validate(json_data)

            if errors:
                return error_response(422, errors)

            return fn(*args, **kwargs)
        return wrapper
    return decorator


def get_obj(data, props):
    splitted_property = props.split('.')
    result = data

    for prop in splitted_property:
        result = result.get(prop)

    return result


def logged_in_user_match_id(props=None):
    def decorator(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            json_data = request.get_json().get('data')
            request_user_id = json_data.get('id') if not props else get_obj(json_data, props)

            if g.current_user.id != request_user_id:
                return error_response(403, 'Current user is not allowed to edit the request object')

            return fn(*args, **kwargs)
        return wrapper
    return decorator
