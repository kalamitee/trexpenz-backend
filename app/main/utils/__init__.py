import urllib.parse
import base64
from ast import literal_eval

def url_b64_to_dict(url_b64):
    unquoted = urllib.parse.unquote(url_b64)
    b64_stringified = base64.urlsafe_b64decode(unquoted).decode()

    return literal_eval(b64_stringified)
