from flask import request, make_response, jsonify
from flask_restful import Resource

from app import db

from .decorators import token_required
from .query_parser import QueryParser

from ..errors import get_not_found_message, error_response
from ..errors.exceptions import ApiFilterError

from ..user.user_model import User as UserModel
from ..expense.expense_model import Expense as ExpenseModel, ExpenseType as ExpenseTypeModel
from ..expense.expense_schema import ExpenseSchema, ExpenseTypeSchema
from ..expense.expense_service import ExpenseTypeService, ExpenseService

from ..budget.budget_model import Budget as BudgetModel, BudgetBreakdown as BudgetBreakdownModel
from ..budget.budget_schema import BudgetSchema
from ..budget.budget_service import BudgetService


class UserBudgetList(Resource):

    budget_schema = BudgetSchema()

    method_decorators = {
        'get': [token_required],
        'post': [token_required]
    }

    def get(self, id):
        q = request.args.get('q')

        user = db.session.query(UserModel).get_or_404(id, description=get_not_found_message(UserModel, id))

        try:
            final_query = QueryParser.get_final_query(BudgetModel, user.budgets, q).order_by(BudgetModel.year.desc())
            result = final_query.all()
        except ApiFilterError as err:
            return error_response(400, str(err))

        return make_response(jsonify(self.budget_schema.dump(result, many=True).data), 200)

    def post(self, id):
        json_data = request.get_json()
        data = self.budget_schema.load(json_data).data

        return BudgetService.save(data)


class UserBudget(Resource):

    budget_schema = BudgetSchema()

    method_decorators = {
        'put': [token_required]
    }

    def put(self, id):
        json_data = request.get_json()
        data = self.budget_schema.load(json_data).data

        return BudgetService.save(data)

    def delete(self, user_id, budget_id):
        budget = db.session.query(BudgetModel).get_or_404(budget_id, description=get_not_found_message(BudgetModel, budget_id))

        if budget.user_id != int(user_id):
            message = BudgetModel.__name__ + ' with id = ' + str(budget.id)
            message += ' does not belong to User with id = ' + str(user_id)

            return make_response(jsonify({'message': message}), 422)

        db.session.delete(budget)
        db.session.commit()

        return make_response(jsonify(None), 204)


class UserExpenseList(Resource):

    expense_schema = ExpenseSchema()

    method_decorators = {
        'get': [token_required],
        'post': [token_required]
    }

    def get(self, id):
        q = request.args.get('q')

        user = db.session.query(UserModel).get_or_404(id, description=get_not_found_message(UserModel, id))

        try:
            final_query = QueryParser.get_final_query(ExpenseModel, user.expenses, q)
            result = final_query.all()
        except ApiFilterError as err:
            return error_response(400, str(err))

        return make_response(jsonify(self.expense_schema.dump(result, many=True).data), 200)

    def post(self, id):
        json_data = request.get_json()
        data = self.expense_schema.load(json_data).data

        return ExpenseService.save(data)


class UserExpense(Resource):

    expense_schema = ExpenseSchema()

    method_decorators = {
        'put': [token_required]
    }

    def put(self, id):
        json_data = request.get_json()
        data = self.expense_schema.load(json_data).data

        return ExpenseService.save(data)

    def delete(self, user_id, expense_id):
        expense = db.session.query(ExpenseModel).get_or_404(expense_id, description=get_not_found_message(ExpenseModel, expense_id))

        if expense.user_id != int(user_id):
            message = ExpenseModel.__name__ + ' with id = ' + str(expense.id)
            message += ' does not belong to User with id = ' + str(user_id)

            return make_response(jsonify({'message': message}), 422)

        db.session.delete(expense)
        db.session.commit()

        return make_response(jsonify(None), 204)


class BudgetExpenseList(Resource):

    expense_schema = ExpenseSchema()

    method_decorators = {
        'get': [token_required]
    }

    def get(self, id):
        q = request.args.get('q')

        budget = db.session.query(BudgetModel).get_or_404(id, description=get_not_found_message(BudgetModel, id))

        try:
            final_query = QueryParser.get_final_query(ExpenseModel, budget.expenses, q)
            result = final_query.all()
        except ApiFilterError as err:
            return error_response(400, str(err))

        return make_response(jsonify(self.expense_schema.dump(result, many=True).data), 200)


class BudgetBreakdownExpenseList(Resource):

    expense_schema = ExpenseSchema()

    method_decorators = {
        'get': [token_required]
    }

    def get(self, id):
        q = request.args.get('q')

        budget = db.session.query(BudgetBreakdownModel).get_or_404(id, description=get_not_found_message(BudgetBreakdownModel, id))

        try:
            final_query = QueryParser.get_final_query(ExpenseModel, budget.expenses, q)
            result = final_query.all()
        except ApiFilterError as err:
            return error_response(400, str(err))

        return make_response(jsonify(self.expense_schema.dump(result, many=True).data), 200)


class ExpenseTypeExpenseList(Resource):

    expense_schema = ExpenseSchema()

    method_decorators = {
        'get': [token_required]
    }

    def get(self, id):
        q = request.args.get('q')

        budget = db.session.query(ExpenseTypeModel).get_or_404(id, description=get_not_found_message(ExpenseTypeModel, id))

        try:
            final_query = QueryParser.get_final_query(ExpenseModel, budget.expenses, q)
            result = final_query.all()
        except ApiFilterError as err:
            return error_response(400, str(err))

        return make_response(jsonify(self.expense_schema.dump(result, many=True).data), 200)


class UserExpenseTypeList(Resource):

    expense_type_schema = ExpenseTypeSchema()

    method_decorators = {
        'get': [token_required],
        'post': [token_required]
    }

    def get(self, id):
        q = request.args.get('q')

        user = db.session.query(UserModel).get_or_404(id, description=get_not_found_message(UserModel, id))

        try:
            final_query = QueryParser.get_final_query(ExpenseTypeModel, user.expense_types, q)
            result = final_query.order_by(ExpenseTypeModel.name).all()
        except ApiFilterError as err:
            return error_response(400, str(err))

        return make_response(jsonify(self.expense_type_schema.dump(result, many=True).data), 200)

    def post(self, id):
        json_data = request.get_json()
        data = self.expense_type_schema.load(json_data).data

        return ExpenseTypeService.save(data)


class UserExpenseType(Resource):

    expense_type_schema = ExpenseTypeSchema()

    method_decorators = {
        'put': [token_required],
        'delete': [token_required]
    }

    def put(self, user_id):
        json_data = request.get_json()
        data = self.expense_type_schema.load(json_data).data

        return ExpenseTypeService.save(data)

    def delete(self, user_id, expense_type_id):
        expense_type = db.session.query(ExpenseTypeModel).get_or_404(expense_type_id, description=get_not_found_message(ExpenseTypeModel, expense_type_id))

        if expense_type.user_id != int(user_id):
            message = ExpenseTypeModel.__name__ + ' with id = ' + str(expense_type.id)
            message += ' does not belong to User with id = ' + str(user_id)

            return make_response(jsonify({'message': message}), 422)

        db.session.delete(expense_type)
        db.session.commit()

        return make_response(jsonify(None), 204)
