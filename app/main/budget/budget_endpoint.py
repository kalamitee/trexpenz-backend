from flask import request, make_response, jsonify
from flask_restful import Resource

from app import db

from ..utils.decorators import validate_request, token_required, logged_in_user_match_id
from ..utils.query_parser import QueryParser
from ..errors import error_response, get_not_found_message
from ..errors.exceptions import ApiFilterError
from .budget_schema import BudgetSchema, BudgetBreakdownWithBudgetSchema
from .budget_service import BudgetService, BudgetBreakdownService
from .budget_model import Budget as BudgetModel


class Budget(Resource):

    budget_schema = BudgetSchema()

    method_decorators = {
        'get': [token_required]
    }

    def get(self, id):
        budget = BudgetModel.query.get_or_404(id, description=get_not_found_message(BudgetModel, id))

        return make_response(jsonify(self.budget_schema.dump(budget).data), 200)


class BudgetList(Resource):

    budget_schema = BudgetSchema()

    method_decorators = {
        'get': [token_required],
        'post': [logged_in_user_match_id('user.id'), token_required, validate_request(budget_schema)],
        'put': [logged_in_user_match_id('user.id'), token_required, validate_request(budget_schema)]
    }

    def get(self):
        q = request.args.get('q')

        try:
            final_query = QueryParser.get_final_query(BudgetModel, db.session.query(BudgetModel), q)
            result = final_query.all()
        except ApiFilterError as err:
            return error_response(400, str(err))

        return make_response(jsonify(self.budget_schema.dump(result, many=True).data), 200)

    def post(self):
        json_data = request.get_json()
        data = self.budget_schema.load(json_data).data

        return BudgetService.save(data)

    def put(self):
        json_data = request.get_json()
        data = self.budget_schema.load(json_data).data

        return BudgetService.save(data)


class BudgetBreakdown(Resource):

    budget_breakdown_with_budget_schema = BudgetBreakdownWithBudgetSchema()

    method_decorators = {
        'put': [token_required, validate_request(budget_breakdown_with_budget_schema)]
    }

    def put(self):
        json_data = request.get_json()
        data = self.budget_breakdown_with_budget_schema.load(json_data).data

        return BudgetBreakdownService.save(data)
