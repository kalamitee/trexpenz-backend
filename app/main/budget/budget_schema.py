from decimal import ROUND_HALF_UP

import simplejson
from marshmallow import fields, validates, ValidationError, pre_load

from ..base import BaseSchema
from ..utils.validators import must_not_be_blank
from ..user.user_schema import UserSchema


class BudgetBreakdownSchema(BaseSchema):
    class Meta:
        json_module = simplejson

    id = fields.Integer(allow_none=True)
    month = fields.Integer(required=True)
    amount = fields.Decimal(places=2, rounding=ROUND_HALF_UP)

    def __init__(self, check_envelope=True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.check_envelope = check_envelope

    @validates('month')
    def validate_month(self, value):
        if value < 1 or value > 12:
            raise ValidationError('Value out of range: valid range is from 1 to 12')


class BudgetSchema(BaseSchema):
    class Meta:
        json_module = simplejson

    id = fields.Integer(allow_none=True)
    name = fields.String(required=True, validate=must_not_be_blank)
    amount = fields.Decimal(required=True, validate=must_not_be_blank)
    year = fields.Integer(required=True, validate=must_not_be_blank)
    date_created = fields.Date(format='%Y-%m-%d', dump_only=True)
    budget_breakdown = fields.Nested(BudgetBreakdownSchema(check_envelope=False, many=True), load_from='budgetBreakdown', dump_to='budgetBreakdown', many=True, missing=[])
    user = fields.Nested(UserSchema(check_envelope=False, only=('id',)), required=True, validate=must_not_be_blank)

    def __init__(self, check_envelope=True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.check_envelope = check_envelope

    @validates('year')
    def validate_year(self, value):
        if len(str(value)) != 4:
            raise ValidationError('It seems like ' + str(value) + ' is not a valid year. Am I right?')


class BudgetBreakdownWithBudgetSchema(BudgetBreakdownSchema):
    id = fields.Integer(required=True, validate=must_not_be_blank)
    # budget = fields.Nested(BudgetSchema, only=('id',), required=True, validate=must_not_be_blank)
