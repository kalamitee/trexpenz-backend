from decimal import Decimal

from sqlalchemy.exc import IntegrityError
from flask import jsonify, make_response

from app import db

from ..errors import get_not_found_message, integrity_error, error_response
from ..errors.exceptions import EditError
from .budget_schema import BudgetSchema, BudgetBreakdownWithBudgetSchema
from .budget_model import Budget as BudgetModel, BudgetBreakdown as BudgetBreakdownModel


class BudgetBreakdownService:
    @staticmethod
    def save(data):
        response = BudgetBreakdownModel.from_config(data)

        db.session.commit()

        return make_response(jsonify(BudgetBreakdownWithBudgetSchema().dump(response).data), 200)


class BudgetService:
    @staticmethod
    def save(data):
        status_code = None
        response = None
        schema = BudgetSchema()

        try:
            response = BudgetModel.from_config(data)
            # response.budget_breakdown = response.budget_breakdown.order_by(BudgetBreakdownModel.month.asc())

            # print('THE RESPONSE ', response.budget_breakdown.order_by(BudgetBreakdownModel.month.asc()).all())

            if data.get('id'):
                status_code = 200
                db.session.add(response)
            else:
                status_code = 201

            db.session.commit()
        except IntegrityError as err:
            return integrity_error(err)
        except EditError as err:
            db.session.rollback()
            return error_response(422, str(err))

        return make_response(jsonify(schema.dump(response).data), status_code)
