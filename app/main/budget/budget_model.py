import datetime

from app import db

from .. .main.errors import get_not_found_message
from .. .main.errors.exceptions import EditError
from .. .main.user.user_model import User as UserModel


class BudgetBreakdown(db.Model):

    __table_args__ = (
        db.UniqueConstraint('budget_id', 'month', name='unique_year_month'),
    )

    id = db.Column(db.Integer, primary_key=True)
    budget_id = db.Column(db.Integer, db.ForeignKey('budget.id'), nullable=False, index=True)
    amount = db.Column(db.Numeric(), nullable=False, index=True)
    month = db.Column(db.Integer, index=True)
    expenses = db.relationship('Expense', backref='budget_breakdown', lazy='dynamic')

    def set_data(self, data):
        setattr(self, 'amount', data.get('amount'))
        setattr(self, 'month', data.get('month'))

    @classmethod
    def from_config(cls, data):
        budget_breakdown = cls.query.get_or_404(data.get('id'), description=get_not_found_message(cls, data.get('id')))
        budget_breakdown.set_data(data)

        return budget_breakdown


class Budget(db.Model):

    __table_args__ = (
        db.UniqueConstraint('user_id', 'year', name='unique_user_id_year'),
    )

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.Date, nullable=False, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)
    year = db.Column(db.String(4), nullable=False, index=True)
    name = db.Column(db.String(64), nullable=False, index=True)
    amount = db.Column(db.Numeric(), nullable=False, index=True)
    budget_breakdown = db.relationship('BudgetBreakdown', backref='budget', lazy='dynamic', order_by='BudgetBreakdown.month', cascade='all,delete')
    expenses = db.relationship('Expense', backref='budget', lazy='dynamic')

    def set_data(self, data):
        if not data.get('id'):
            setattr(self, 'date_created', datetime.date.today())

        setattr(self, 'year', data.get('year'))
        setattr(self, 'name', data.get('name'))
        setattr(self, 'amount', data.get('amount'))

        user = UserModel.query.get_or_404(data['user']['id'], description=get_not_found_message(UserModel, data['user']['id']))
        setattr(self, 'user', user)

        self.save_budget_breakdown(data.get('budget_breakdown'))

    def save_budget_breakdown(self, budget_breakdown_data):
        to_create = [d for d in budget_breakdown_data if 'id' not in d.keys() or not d.get('id')]
        to_update = [d for d in budget_breakdown_data if 'id' in d.keys() and d.get('id')]

        self.create_budget_breakdown_list(to_create)

        if not self.id and len(to_update):
            raise EditError('Budget breakdown with id cannot be attached to a budget that has no id')
        else:
            self.update_budget_breakdown_list(to_update)

    def create_budget_breakdown_list(self, to_create_budget_breakdown):
        for d in to_create_budget_breakdown:
            self.budget_breakdown.append(BudgetBreakdown(**d))

    def update_budget_breakdown_list(self, to_update_budget_breakdown):
        for d in to_update_budget_breakdown:
            budget_breakdown = BudgetBreakdown.query.get_or_404(d.get('id'), description=get_not_found_message(BudgetBreakdown, d.get('id')))

            if budget_breakdown.budget_id != self.id:
                raise EditError('Budget breakdown ' + str(budget_breakdown.id) + ' does not belong to budget ' + str(self.id))

            budget_breakdown.set_data(d)

    @classmethod
    def from_config(cls, data):
        if data.get('id'):
            budget = cls.query.get_or_404(data.get('id'), description=get_not_found_message(cls, data['id']))
        else:
            budget = cls()

        budget.set_data(data)

        return budget
